
namespace StardewValleyEasyPrairieKing 
{
   public class ModConfig
    {

        public bool alwaysInvincible{ get; set; } = true;
        public int lives { get; set; } = 99;
        public int coins { get; set; } = 99;
        public int ammoLevel { get; set; } = 0;
        public int bulletDamage { get; set; } = 0;
        public int fireSpeedLevel { get; set; } = 5;
        public int runSpeedLevel { get; set; } = 2;
        public bool useShotgun { get; set; } = true;
        public bool useWheel { get; set;  } = false;
        public bool useRapidFire {get;set;} = false;
        // Seems like the following 3 upgrades don't do anything. commented out for the time being.
        // public bool useSheriff {get;set;} = false;
        // public bool useNuke {get;set;} = false;
        // public bool useZombie {get;set;} = false;
        public int waveTimer { get; set; } = 
                                            #if DEBUG
                                                10;
                                            #else 
                                                60;
                                                #endif
        public int timeBetweenWaves {get;set;} = 5;
        
    }
}