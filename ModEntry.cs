﻿
using System;
using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewModdingAPI.Utilities;
using StardewValley;
using StardewValley.Minigames;

using System.Reflection;

namespace StardewValleyEasyPrairieKing
{
    /// <summary>The mod entry point.</summary>
    internal sealed class ModEntry : Mod
    {



        ModConfig Config = new ModConfig(); // disable warning because this is not initalized during ModEntry constructor.

        const string PrairieKingID = "PrairieKing";
        const int DefaultWaveTimer = 80; //seconds. The game itself uses milliseconds so the value is actually 80000.
        const int Infinite = 100;
        bool _coinsSet = false;
        bool _livesSet = false;
        /*********
        ** Public methods
        *********/
        /// <summary>The mod entry point, called after the mod is first loaded.</summary>
        /// <param name="helper">Provides simplified APIs for writing mods.</param>
        public override void Entry(IModHelper helper)
        {
            Config = helper.ReadConfig<ModConfig>();

            AddConsoleCommands();

            helper.Events.GameLoop.GameLaunched += OnGameLaunched;
            helper.Events.GameLoop.UpdateTicked += EventUpdateTicking;           
        }

        void AddConsoleCommands()
        {
            Helper.ConsoleCommands.Add("prairie_save", "Save the current configuration permanently", (notUsed, notUsed2) => Helper.WriteConfig(Config) );
            try
            {
                PropertyInfo[] properties = Config.GetType().GetProperties();

                foreach (PropertyInfo property in properties)
                {
                    var name = property.Name;
                    Helper.ConsoleCommands.Add($"prairie_{name.ToLower()}", "", (cmdName, arg) => SetValue(name, arg) );
                }
            }
            catch(Exception ex)
            {
                Monitor.Log($"EasyPrairieKing: Failed to add console commands: {ex.Message}", LogLevel.Error);
            }
        }

        void SetValue(string name, string[] value)
        {
            if(value.Length < 1)
            {
                return;
            }

            try
            {   
                var property = Config?.GetType()?.GetProperty(name);
                if(property != null) 
                {
                    property.SetValue(Config, Convert.ChangeType(value[0], property.PropertyType) );
                }

            }
            catch(Exception ex)
            {
                Monitor.Log($"EasyPrairieKing: Failed to set {name} to {value}  commands: {ex.Message}", LogLevel.Error);
            }
        }


        void OnWarped(object? sender, EventArgs e)
        {
            // Game1.player
            var miniGame = Game1.currentMinigame as AbigailGame;
            if(miniGame != null)
            {
                miniGame.coins = Config.coins;
                miniGame.lives = Config.lives;
            }
        }

        private void OnGameLaunched(object? sender, EventArgs e)
        {
            // get Generic Mod Config Menu's API (if it's installed)
            var configMenu = this.Helper.ModRegistry.GetApi<GenericModConfigMenu.IGenericModConfigMenuApi>("spacechase0.GenericModConfigMenu");
            if (configMenu is null)
                return;

            // register mod
            configMenu.Register(
                mod: this.ModManifest,
                reset: () => this.Config = new ModConfig(),
                save: () => this.Helper.WriteConfig(this.Config)
            );

            configMenu.AddSectionTitle(ModManifest, text: () => "General");

      
            configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Lives", getValue: () => this.Config.lives,
                            setValue: value => Config.lives = value, min:1, max: Infinite, 
                            formatValue: value => value == Infinite ? "infite" : value.ToString());

            configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Coins", getValue: () => this.Config.coins,
                            setValue: value => Config.coins = (int) value, min:0, max: Infinite, 
                            formatValue: value => value == Infinite ? "infite" : value.ToString() );

            configMenu.AddSectionTitle(ModManifest, text: () => "Timers");

            configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Wave timer", getValue: () => this.Config.waveTimer,
                            tooltip: () => "Set the maximum time for each wave in seconds. Default is 80 seconds.",
                            setValue: value => Config.waveTimer = (int) value, min:1, //max: 100, 
                            formatValue: value => $"{value} seconds" );

            configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Time between waves", getValue: () => this.Config.timeBetweenWaves,
                            tooltip: () => "Set the maximum time for each wave in seconds. Default is 5 seconds.",
                            setValue: value => Config.timeBetweenWaves = (int) value, min:1);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Upgrades
        const int maxUpgradeLevel = 10;


        configMenu.AddSectionTitle(ModManifest, text: () => "Upgrades");

          

        configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Ammo level", 
                        tooltip: () => "Sets the power of your ammo.",
                        getValue: () => this.Config.ammoLevel,                        
                        setValue: value => Config.ammoLevel = (int) value, min:0, max: maxUpgradeLevel
                        );

        configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Bullet damage", 
                        tooltip: () => "Sets the damage of your bullets.",
                        getValue: () => this.Config.bulletDamage,                        
                        setValue: value => Config.bulletDamage = (int) value, min:0, max: maxUpgradeLevel
                        );

        configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Fire speed", 
                        // tooltip: () => "Sets the power of your ammo.",
                        getValue: () => this.Config.fireSpeedLevel,                        
                        setValue: value => Config.fireSpeedLevel = (int) value, min:0, max: maxUpgradeLevel
                        );

        configMenu.AddNumberOption(mod : this.ModManifest, name: () => "Movement speed", 
                        // tooltip: () => "Sets the power of your ammo.",
                        getValue: () => this.Config.runSpeedLevel,                        
                        setValue: value => Config.runSpeedLevel = (int) value, min:0, max: maxUpgradeLevel
                         );

        configMenu.AddSectionTitle(ModManifest, text: () => "PowerUps");

        // add some config options
        configMenu.AddBoolOption(
            mod: this.ModManifest,
            name: () => "Always invincible",
            //tooltip: () => "An optional description shown as a tooltip to the player.",
            getValue: () => this.Config.alwaysInvincible,
            setValue: value => this.Config.alwaysInvincible = value
        );
        // add some config options
        configMenu.AddBoolOption(
            mod: this.ModManifest,
            name: () => "Always shotgun",
            //tooltip: () => "An optional description shown as a tooltip to the player.",
            getValue: () => this.Config.useShotgun,
            setValue: value => this.Config.useShotgun = value
        );
        // add some config options
        configMenu.AddBoolOption(
            mod: this.ModManifest,
            name: () => "Always spread pistol",
            //tooltip: () => "An optional description shown as a tooltip to the player.",
            getValue: () => this.Config.useWheel,
            setValue: value => this.Config.useWheel = value
        );    

        // add some config options
        configMenu.AddBoolOption(
            mod: this.ModManifest,
            name: () => "Always use rapid fire",
            //tooltip: () => "An optional description shown as a tooltip to the player.",
            getValue: () => this.Config.useRapidFire,
            setValue: value => this.Config.useRapidFire = value
        );

        configMenu.AddSectionTitle(ModManifest, text: () => "Instantly unlock achievements");
        
        configMenu.AddBoolOption(ModManifest, name: () => "Instantly unlock \nPrairie King achievement", tooltip: () => "This feature is untested. Unknown if it works",
        getValue: () => { return false; } , setValue: value => { if(value) {Game1.getSteamAchievement("Achievement_PrairieKing");} }  );
        configMenu.AddBoolOption(ModManifest, name: () => "Instantly unlock \nFectors Challenge achievement", tooltip: () => "This feature is untested. Unknown if it works",
        getValue: () => { return false; } , setValue: value => { if(value) {Game1.getSteamAchievement("Achievement_FectorsChallenge");} }  );

                // Game1.getSteamAchievement("Achievement_PrairieKing");
                // Game1.getSteamAchievement("Achievement_FectorsChallenge");

        // // add some config options
        // configMenu.AddBoolOption(
        //     mod: this.ModManifest,
        //     name: () => "Always use sheriff",
        //     //tooltip: () => "An optional description shown as a tooltip to the player.",
        //     getValue: () => this.Config.useSheriff,
        //     setValue: value => this.Config.useSheriff = value
        // );
        // // add some config options
        // configMenu.AddBoolOption(
        //     mod: this.ModManifest,
        //     name: () => "Always use nuke",
        //     //tooltip: () => "An optional description shown as a tooltip to the player.",
        //     getValue: () => this.Config.useNuke,
        //     setValue: value => this.Config.useNuke = value
        // );    
        // configMenu.AddBoolOption(
        //     mod: this.ModManifest,
        //     name: () => "Always use zombie",
        //     //tooltip: () => "An optional description shown as a tooltip to the player.",
        //     getValue: () => this.Config.useZombie,
        //     setValue: value => this.Config.useZombie = value
        // );        

        }

        private void EventUpdateTicking(object? sender,  EventArgs e)
        {
            var prairieGame = Game1.currentMinigame as AbigailGame;
            if(prairieGame == null)
            {
                _livesSet = false;
                _coinsSet = false;
                return;
            }

            int waveTimer = Config.waveTimer * 1000;
            const int upgradeTimer = 10000;


            try
            {
                var jotpkProgress = Game1.player.jotpkProgress.Value;

                AbigailGame.betweenWaveTimer = Math.Min(Config.timeBetweenWaves * 1000, AbigailGame.betweenWaveTimer);

                if(AbigailGame.waveTimer != 0 && AbigailGame.waveTimer > waveTimer 
                    || (jotpkProgress?.waveTimer.Value != 0 && jotpkProgress?.waveTimer.Value > waveTimer) )
                {
                    AbigailGame.waveTimer = waveTimer;
                    if(jotpkProgress != null) 
                    { 
                        jotpkProgress.waveTimer.Value = waveTimer;
                    }
                }

                if(Config.useShotgun)
                {
                    prairieGame.activePowerups[AbigailGame.POWERUP_SHOTGUN] = upgradeTimer;
                }
                if(Config.useWheel)
                {
                    prairieGame.activePowerups[AbigailGame.POWERUP_SPREAD] = upgradeTimer;
                    prairieGame.spreadPistol = true;
                }
                if(Config.alwaysInvincible)
                {
                    AbigailGame.playerInvincibleTimer = upgradeTimer;
                    prairieGame.player2invincibletimer = upgradeTimer;
                }

                if(Config.useRapidFire)
                {
                    prairieGame.activePowerups[AbigailGame.POWERUP_RAPIDFIRE] = upgradeTimer;
                }
                // if(Config.useSheriff)
                // {
                //     miniGame.activePowerups[AbigailGame.POWERUP_SHERRIFF] = upgradeTimer;
                // }
                // if(Config.useSheriff)
                // {
                //     miniGame.activePowerups[AbigailGame.POWERUP_NUKE] = upgradeTimer;
                // }
                // if(Config.useZombie)
                // {
                //     miniGame.activePowerups[AbigailGame.POWERUP_ZOMBIE] = upgradeTimer;
                // }

                if(Config.lives >= Infinite)
                {
                    prairieGame.lives = Config.lives;
                    if(jotpkProgress != null) jotpkProgress.lives.Value = Config.lives;
                }
                else if(!_livesSet)
                {
                    _livesSet = true;
                    prairieGame.lives = Config.lives;
                }
                if(Config.coins >= Infinite)
                {
                    prairieGame.coins = Config.coins;
                    if(jotpkProgress != null) jotpkProgress.coins.Value = Config.coins;
                }
                else if(!_coinsSet)
                {
                    _coinsSet = true;
                    prairieGame.coins = Config.coins;
                }



                prairieGame.bulletDamage = Math.Max(Config.bulletDamage, prairieGame.bulletDamage);
                prairieGame.ammoLevel = Math.Max(Config.ammoLevel, prairieGame.ammoLevel);
                prairieGame.fireSpeedLevel = Math.Max(Config.fireSpeedLevel, prairieGame.fireSpeedLevel);
                prairieGame.runSpeedLevel = Math.Max(Config.runSpeedLevel, prairieGame.runSpeedLevel);
                
                if(jotpkProgress != null)
                {
                    jotpkProgress.bulletDamage.Value = Math.Max(prairieGame.bulletDamage, jotpkProgress.bulletDamage.Value);
                    jotpkProgress.ammoLevel.Value = Math.Max(prairieGame.ammoLevel, jotpkProgress.ammoLevel.Value);
                    jotpkProgress.fireSpeedLevel.Value = Math.Max(prairieGame.fireSpeedLevel, jotpkProgress.fireSpeedLevel.Value);
                    jotpkProgress.runSpeedLevel.Value = Math.Max(prairieGame.runSpeedLevel, jotpkProgress.runSpeedLevel.Value);

                    prairieGame.bulletDamage = jotpkProgress.bulletDamage.Value;
                    prairieGame.ammoLevel = jotpkProgress.ammoLevel.Value;
                    prairieGame.fireSpeedLevel = jotpkProgress.fireSpeedLevel.Value;
                    prairieGame.runSpeedLevel = jotpkProgress.runSpeedLevel.Value;
                }
                
            }
            catch(Exception ex)
            {
                Monitor.Log($"EasyPrairieKing: Failed to set prairie king minigame values: {ex.Message}", LogLevel.Error);
            }

        }

    }
}
